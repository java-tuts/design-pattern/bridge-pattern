package com.d4z.request;

import io.vertx.core.json.JsonObject;

public class ApiCommentHandler extends ApiRequestHandler {
	public ApiCommentHandler(IApiMethodHandler handler) {
		this.handler = handler;
	}
	
	@Override
	public JsonObject handle() {
		JsonObject lvResp = this.handler.handle();
		lvResp.put("endpoint", "/api/comment");
		return lvResp;
	}
}
