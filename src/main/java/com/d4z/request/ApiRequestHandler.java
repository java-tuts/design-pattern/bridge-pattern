package com.d4z.request;

import io.vertx.core.json.JsonObject;

public abstract class ApiRequestHandler {
	protected IApiMethodHandler handler;
	
	public abstract JsonObject handle();
}
