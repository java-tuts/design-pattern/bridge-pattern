package com.d4z.request;

import io.vertx.core.json.JsonObject;

public class ApiUserHandler extends ApiRequestHandler {
	public ApiUserHandler(IApiMethodHandler handler) {
		this.handler = handler;
	}
	
	@Override
	public JsonObject handle() {
		JsonObject lvResp = this.handler.handle();
		lvResp.put("endpoint", "/api/user");
		return lvResp;
	}
}
