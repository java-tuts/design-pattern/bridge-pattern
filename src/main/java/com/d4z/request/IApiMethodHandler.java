package com.d4z.request;

import io.vertx.core.json.JsonObject;

public interface IApiMethodHandler {
	public JsonObject handle();
}
