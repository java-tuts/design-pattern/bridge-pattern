package com.d4z.request;

import io.vertx.core.json.JsonObject;

public class ApiMethodGetHandler implements IApiMethodHandler {
	
	@Override
	public JsonObject handle() {
		return new JsonObject().put("method", "GET");
	}
}
