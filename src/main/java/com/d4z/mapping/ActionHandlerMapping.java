package com.d4z.mapping;

import java.util.HashMap;
import java.util.Map;

public enum ActionHandlerMapping {
	INSTANCE;
	
	private Map<String, String> requestHandlerMapping;
	private Map<String, String> methodHandlerMapping;
	
	ActionHandlerMapping() {
		requestHandlerMapping = new HashMap<String, String>();
		requestHandlerMapping.put("user", "com.d4z.request.ApiUserHandler");
		requestHandlerMapping.put("comment", "com.d4z.request.ApiCommentHandler");
		

		methodHandlerMapping = new HashMap<String, String>();
		methodHandlerMapping.put("GET", "com.d4z.request.ApiMethodGetHandler");
		methodHandlerMapping.put("POST", "com.d4z.request.ApiMethodPostHandler");
	}
	
	public String getRequestHandlerMapping(String pAction) {
		return requestHandlerMapping.get(pAction);
	}
	
	public String getMethodHandlerMapping(String pMethod) {
		return methodHandlerMapping.get(pMethod);
	}
}
